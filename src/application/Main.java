package application;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import dataSave.LoadData;
import dataSave.SaveData;
import model.Guitar;
import model.Strings;

public class Main {

	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
		Guitar g1 = new Guitar(6, "eko", new Strings("ernieball", 7));
		Guitar g2 = new Guitar(7, "Schecty", new Strings("d'addario", 9));
		
		SaveData save = new SaveData(g1, "src/datasaved/Guitar.txt");
		save.write();
		
		SaveData save2 = new SaveData(g2, "src/datasaved/Guitar2.txt");
		save2.write();
		
		LoadData data = new LoadData("src/datasaved/Guitar.txt");
		Guitar gSaved = (Guitar) data.getData();
		System.out.println("name " + gSaved.getName());
		System.out.println("String name " + gSaved.getStringName());
		System.out.println("String number " + gSaved.getStringNumber());
	}

}
