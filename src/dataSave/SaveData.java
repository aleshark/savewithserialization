package dataSave;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import model.Guitar;

public class SaveData {
	
	private final Guitar g1;
	private ObjectOutputStream out;
	
	public SaveData(final Guitar g1, final String pathSavedFile) throws FileNotFoundException, IOException {
		this.g1 = g1;
		this.out = new ObjectOutputStream(new FileOutputStream(pathSavedFile));
	}
	
	public void write() throws IOException {
		out.writeObject(g1);
	}
}
