package model;

import java.io.Serializable;

public class Guitar implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final int stringNumber;
	private final String name;
	private final Strings stringType;
	
	public Guitar(final int stringNumber, final String name, final Strings stringType) {
		this.stringNumber = stringNumber;
		this.name = name;
		this.stringType = stringType;
	}
	
	public String getStringNumber() {
		return Integer.toString(stringNumber);
	}
	
	public String getName() {
		return name;
	}
	
	public String getStringName() {
		return (stringType.getName());
	}
	
}
