package dataSave;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;


public class LoadData {
	
	private final ObjectInputStream in;
	
	public LoadData(final String pathSavedFile) throws FileNotFoundException, IOException {
		this.in = new ObjectInputStream(new FileInputStream(pathSavedFile));
	}
	
	public Object getData() throws ClassNotFoundException, IOException {
		return in.readObject();
	}
	
}
